FROM 572990546101.dkr.ecr.ap-south-1.amazonaws.com/mvn-build:latest as BUILD

COPY . /root/myapp

RUN mvn -X -f /root/myapp/pom.xml clean package


FROM 572990546101.dkr.ecr.ap-south-1.amazonaws.com/wm-hardened-tomcat:latest

# Take the war and copy to webapps of tomcat

COPY --from=BUILD /root/myapp/target/*SNAPSHOT.war /usr/local/tomcat/webapps/
