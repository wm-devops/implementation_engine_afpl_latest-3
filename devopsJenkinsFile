currentBuild.displayName = "ImplementationEngineBuild#" + currentBuild.number
pipeline {
    agent any

    parameters {
      choice choices: ['devops_2.0', 'master_wf_2.0', 'develop2', 'edge2', 'stable2', 'master_wf_2.0-LICP-4803'], description: 'Select Branch for Deployment', name: 'branch'
      choice choices: ['journey', 'portal', 'masters', 'configurator', 'default'], description: 'Select Profile to Build', name: 'profile'
    }
    
    environment {
      containerRegistryPath = '612359323314.dkr.ecr.ap-south-1.amazonaws.com'
      dockerImagePath = "${containerRegistryPath}/journey:${branch}"
    }

    stages {
        stage("Code Checkout") {
            steps {
                git branch: "${branch}", credentialsId: 'lendin-dev-bitbucket-creds', url: 'https://bitbucket.org/kulizadev/implementation_engine.git'

              bitbucketStatusNotify(buildState: 'INPROGRESS')

            }
        }

                
        stage("Build LendInCore Repo") {
            steps {
                build job: 'maven-build-validation-pipeline', parameters: [
                string(name: 'buildProfile', value: "deploy"),
                string(name: 'BRANCH', value: "${branch}"),
                string(name: 'PROFILE', value: "${profile}")
                ]
            }
        } 
        stage("Compile-Package") {
            when {
                expression { 
                    params.profile ==~ /journey|portal|default/
                }
            }
            steps {
                sh "mvn -U clean package -Dmaven.test.skip=true"
                sh "rm -rf target/workbench*exec.war"
                sh "mv target/workbench*.war  target/journey.war"
            }
        } /*
        stage("SonarQube Analysis") {
            when {
                expression { 
                    params.profile ==~ /journey|portal/
                }
            }
            steps {
                withSonarQubeEnv('sonar-jenkins-poc') {
                    sh "mvn sonar:sonar -Dmaven.test.skip=true -Dsonar.projectName=IE-${profile}-${branch}"
                }
            }
        }
        stage("Sonar Quality Gate") {
            when {
                expression { 
                    params.profile ==~ /journey|portal/
                }
            }
            steps {
                script {
                    def qualitygate = waitForQualityGate()
                    if (qualitygate.status != "OK") {
                        slackSend baseUrl: 'https://hooks.slack.com/services/',
                            channel: '#jenkins-poc',
                            color: 'danger',
                            message: "Sonar Quality gate failed with status: ${qualitygate.status}",
                            teamDomain: 'kuliza-talk',
                            tokenCredentialId: 'jenkins-poc-slack-token'
                        error "Pipeline aborted due to quality gate coverage failure: ${qualitygate.status}"
                    }
                }
            }
        } */
        stage("Download War") {
            when {
                expression { 
                    params.profile ==~ /masters|configurator/
                }
            }
            steps {

                sh "mvn org.apache.maven.plugins:maven-dependency-plugin:2.4:get -U -DartifactId=${profile} -DgroupId=com.kuliza.lending -Dversion=2.0.0-SNAPSHOT -Dpackaging=war -Ddest=target/${profile}.war"
            }
        }
        stage("Push to AWS Registry") {
            steps {
                withDockerRegistry(credentialsId: 'ecr:ap-south-1:product-aws-ecr', url: 'https://${containerRegistryPath}/journey') {
                    script {
                        def newImg = docker.build "${dockerImagePath}"
                        newImg.push()
                    }
                }
            }
        }
        stage("Remove Local Docker Image") {
            steps {
                sh "docker rmi ${dockerImagePath}"
            }
        }
    }
    post {
        always {
            /*
            mail bcc: '',
                body: "${currentBuild.currentResult}: Job - ${env.JOB_NAME} | Build - ${env.BUILD_NUMBER} \nFor more info visit: : ${env.BUILD_URL}console \nThanks,\nLendin Jenkins",
                cc: '',
                from: '',
                replyTo: '',
                subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.BUILD_DISPLAY_NAME} ",
                to: 'shrey.bhasin@kuliza.com,gaurav.jadhav@kuliza.com,manjunath.c@kuliza.com'
            */
            slackSend baseUrl: 'https://hooks.slack.com/services/',
                    channel: '#jenkins-poc',
                    color: 'good',
                    message: "${currentBuild.currentResult}: Job - ${env.BUILD_DISPLAY_NAME} \nFor more info visit: : ${env.BUILD_URL}console \nThanks,\nLendin Jenkins",
                    teamDomain: 'kuliza-talk',
                    tokenCredentialId: 'jenkins-poc-slack-token'
        cleanWs()
        script {
            if ('SUCCESS' == currentBuild.currentResult) {
                bitbucketStatusNotify(buildState: 'SUCCESSFUL')
            } else {
                bitbucketStatusNotify(buildState: 'FAILED')
            }
        }
        }
    }
}
